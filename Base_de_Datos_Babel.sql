-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.4.20-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Volcando estructura de base de datos para babel
DROP DATABASE IF EXISTS `babel`;
CREATE DATABASE IF NOT EXISTS `babel` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `babel`;

-- Volcando estructura para tabla babel.hibernate_sequence
DROP TABLE IF EXISTS `hibernate_sequence`;
CREATE TABLE IF NOT EXISTS `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla babel.hibernate_sequence: 1 rows
DELETE FROM `hibernate_sequence`;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` (`next_val`) VALUES
	(1);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;

-- Volcando estructura para tabla babel.ubicacion
DROP TABLE IF EXISTS `ubicacion`;
CREATE TABLE IF NOT EXISTS `ubicacion` (
  `id_ubicacion` bigint(20) NOT NULL,
  `estante` int(11) NOT NULL,
  `librero` int(11) NOT NULL,
  `posicion` int(11) NOT NULL,
  `sala` int(11) NOT NULL,
  PRIMARY KEY (`id_ubicacion`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla babel.ubicacion: 46 rows
DELETE FROM `ubicacion`;
/*!40000 ALTER TABLE `ubicacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `ubicacion` ENABLE KEYS */;

-- Volcando estructura para tabla babel.volumenes
DROP TABLE IF EXISTS `volumenes`;
CREATE TABLE IF NOT EXISTS `volumenes` (
  `id_volumen` bigint(20) NOT NULL AUTO_INCREMENT,
  `autor` varchar(100) NOT NULL,
  `edicion` varchar(100) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `fk_ubicacion` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_volumen`),
  UNIQUE KEY `UK_kdty578p6ia3jbxh2vulgi153` (`nombre`),
  KEY `FKicfup387smkxkj2u3dbhel76` (`fk_ubicacion`)
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla babel.volumenes: 7 rows
DELETE FROM `volumenes`;
/*!40000 ALTER TABLE `volumenes` DISABLE KEYS */;
/*!40000 ALTER TABLE `volumenes` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
